import 'package:dart_exercise/dart_exercise.dart' as dart_exercise;

import 'dart:io';

class Calculator {
  double num1 = 0.0;
  double num2 = 0.0;
  String symbol = '';
  double resule = 0.0;

  Calculator(double num1, double num2, String symbol) {
    this.num1 = num1;
    this.num2 = num2;
    this.symbol = symbol;
  }
  double caculator() {
    if (symbol == "1") {
      resule = num1 + num2;
    } else if (symbol == "2") {
      resule = num1 - num2;
    } else if (symbol == "3") {
      resule = num1 * num2;
    } else if (symbol == "4") {
      resule = num1 / num2;
    } else {
      print("Invalid Choice");
    }
    return resule;
  }
}

void main() {
  stdout.write("Please input number 1 : ");
  double? num1 = double.parse(stdin.readLineSync()!);
  stdout.write("Please input number 2 : ");
  double? num2 = double.parse(stdin.readLineSync()!);
  String message = '''
  Select Operation
  1. Add
  2. Sub
  3. Mul
  4. Div
  Yourt choice ? ''';
  stdout.write(message);
  stdout.write("Please input symbol : ");
  String? symbol = stdin.readLineSync()!;
  Calculator calculator = Calculator(num1, num2, symbol);
  stdout.write('Resule: ');
  stdout.write(calculator.caculator());
}
